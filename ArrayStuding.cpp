#include <iostream>

int main()
{   
    const int N = 10;
    int myArray[N][N];

    // ��������� ������ ����������, 
    // ������� ����� ������� ������ � �������:
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            // ���������� ������ ������ � ������� �������� 
            myArray[i][j] = i + j;

            // ������� ������ ������� ������� ���������
            std::cout << myArray[i][j] << " ";
            if (myArray[i][j] < 10)
                std::cout << " ";
        }
        std::cout << std::endl;
    }

    // �������������� ���������� �������� ������� ������:
    int currentDay = 31;
    int dayNDivision = currentDay % N;
    int lineSum = 0;

    // �������� ���������� ������ � ��������� ���������� � �� ���������:
    std::cout << "\n"\
        << "Current day = " << currentDay << "\n"\
        << "Array lines length = " << N << "\n"\
        << "Remainder of " << currentDay << " / " << N  << " = " << dayNDivision << "\n"\
        << std::endl;

    // ���������� ��� �������� ������ ��� ������� dayNDivision
    for (int i = 0; i < N; i++)
    {
        lineSum = lineSum + myArray[dayNDivision][i];
    }

    // �������� ����� ��������� ������������ ������
    std::cout << "The sum of line #" << dayNDivision << " elements = " << lineSum << "\n" << std::endl;
}